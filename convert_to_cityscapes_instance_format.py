from __future__ import print_function, absolute_import, division, unicode_literals
import os
import numpy as np
from PIL import Image
import json
import sys

def _ensure_dir(dir_path):
    try:
        os.mkdir(dir_path)
    except FileExistsError:
        pass

paths = ['./mapilaryvistas/train/train/annotations', './mapilaryvistas/Test/Test/annotations']
for path in paths:
    save_path = path.replace('annotations','cityscapes_instance_format')
    _ensure_dir(save_path)
    files = os.listdir(path)
    print(path)
    for progress, f in enumerate(files):
        originalFormat = np.array(Image.open(os.path.join(path,f)))
        unique_pixels = np.vstack({tuple(r) for r in originalFormat.reshape(-1,3)})
        saveFormat = np.zeros(originalFormat.shape[0:2], np.uint16)
        for color in unique_pixels:   
            segmentId = color[2]*256*256+color[1]*256+color[0]
            mask = np.logical_and(np.logical_and(originalFormat[:,:,0]==color[0],originalFormat[:,:,1]==color[1]),originalFormat[:,:,2]==color[2])
            saveFormat[mask] = segmentId
        Image.fromarray(saveFormat).save(os.path.join(save_path, f))
        print("\rProgress: {:>3.2f} %".format((progress + 1) * 100 / len(files)), end=' ')
        sys.stdout.flush()
